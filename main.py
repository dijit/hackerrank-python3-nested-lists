def main():
    _all = []
    # real main:
    for _ in range(int(input())):
        name = input()
        score = float(input())
        _all.append([name, score])
    sorted_keys = sort_lot(_all)
    stringy = stringify_second_to_last(sorted_keys)
    print(stringy, end='')


def stringify_second_to_last(_in: list) -> str:
    """
    takes the sorted list, checks the value before it and if it's the same
    returns a sorted string
    """
    _remainder: list = []

    scores: set = set([_in[x][1] for x in range(len(_in))])
    scores: list = list(scores)
    scores.sort()

    _remainder = [x[0] for x in _in if x[1] == scores[1]]

    if len(_remainder) > 1:
        _remainder.sort()

    ret_str: str = '\n'.join(_remainder)
    return ret_str


def sort_lot(_in: list, reverse=True) -> list:
    """
    sorts based on index[1]
    """
    _out = sorted(_in, key=lambda x: float(x[1]), reverse=reverse)
    return _out


if __name__ == '__main__':
    main()
