import pytest
from io import StringIO
from main import *

def test_main_orig_problem(monkeypatch, capsys):
    _input = StringIO("""5
Harry
37.21
Berry
37.21
Tina
37.2
Akriti
41
Harsh
39""")
    _output = """Berry\nHarry"""
    monkeypatch.setattr('sys.stdin', _input)
    main()
    captured_stdout, captured_stderr = capsys.readouterr()
    assert captured_stdout == _output

def test_main_non_orig_problem(monkeypatch, capsys):
    _input = StringIO("""5
Harry
37.21
Berry
39.21
Tina
37.2
Akriti
41
Harsh
39""")
    _output = "Harry"
    monkeypatch.setattr('sys.stdin', _input)
    main()
    captured_stdout, captured_stderr = capsys.readouterr()
    assert captured_stdout == _output

# def test_main_out_of_bounds_high(monkeypatch, capsys):
#     _input = StringIO("""6
# Harry
# 37.21
# Berry
# 39.21
# Tina
# 37.2
# Akriti
# 41
# Harsh
# 39
# Jan
# 400""")
#     _output = "Harry"
#     monkeypatch.setattr('sys.stdin', _input)
#     main()
#     captured_stdout, captured_stderr = capsys.readouterr()
#     assert captured_stdout == _output
# 
# 
# def test_main_out_of_bounds_low(monkeypatch, capsys):
#     _input = StringIO("""1
# Harry
# 37.21""")
#     _output = "Harry"
#     monkeypatch.setattr('sys.stdin', _input)
#     main()
#     captured_stdout, captured_stderr = capsys.readouterr()
#     assert captured_stdout == _output


def test_sort_lot():
    _in = [["Harry", 37.21],["Berry", 37.21],["Tina", 37.2],["Akriti",41],["Harsh", 39]]
    _out = [["Akriti",41],["Harsh", 39],["Harry", 37.21],["Berry", 37.21],["Tina", 37.2]]
    sorted_list = sort_lot(_in)
    assert sorted_list == _out


def test_sort_lot_reverse():
    _in = [["Harry", 37.21],["Berry", 37.21],["Tina", 37.2],["Akriti",41],["Harsh", 39]]
    _out = [['Tina', 37.2], ['Harry', 37.21], ['Berry', 37.21], ['Harsh', 39], ['Akriti', 41]]
    sorted_list = sort_lot(_in, reverse=False)
    assert sorted_list == _out


def test_stringyfy_second_to_last_double():
    _in = [['Harry', 37.21],["Berry", 37.21],["Tina", 37.2]]
    _output = """Berry\nHarry"""
    ret = stringify_second_to_last(_in)
    assert ret == _output


def test_stringyfy_second_to_last_single():
    _in = [["Berry", 400.0],['Harry', 37.21],["Tina", 37.2]]
    _output = "Harry"
    ret = stringify_second_to_last(_in)
    assert ret == _output


def test_stringyfy_second_to_last_single_tied_for_last():
    _in = [["Berry", 400.0],['Harry', 37.2],["Tina", 37.2]]
    _output = "Berry"
    ret = stringify_second_to_last(_in)
    assert ret == _output
